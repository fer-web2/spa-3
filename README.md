# cookbook

Različite verzije možete dohvaćati tako da:
  * prvo naravno napravite `git clone git@gitlab.com:fer-web2/spa-3.git`
  * `cd spa-3`
  * zatim checkout verziju koju hoćete (oznake su u predavanjima), npr.:
    *  `git checkout v2.0`
    *  `git checkout v3.0`
    * ...

Za verziju (tag) 4+ pokrenuti i fer-cookbook-backend koji vraća recepte, dakle:

  * `...\fer-cookbook-backend>node server.js`
  * `...\fer-cookbook>npm run serve`

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
